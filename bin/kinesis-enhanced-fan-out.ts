#!/usr/bin/env node
import 'source-map-support/register';
import { App } from '@aws-cdk/core';
import KinesisEnhancedFanOutStack from '../lib/kinesis-enhanced-fan-out-stack';

const app = new App();
new KinesisEnhancedFanOutStack(app, 'KinesisEnhancedFanOutStack');
