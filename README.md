# Kinesis Enhanced Fan-Out Example
This is a small CDK app used to configure [Kinesis Enhanced Fan-Out](https://docs.aws.amazon.com/streams/latest/dev/introduction-to-enhanced-consumers.html). It consists of four components:

1. Kinesis data stream,
2. Kinesis stream consumer,
3. Lambda to put records into the Kinesis data stream, and
4. Lambda to consume messages from the Kinesis stream consumer.

This stack is built and deploying with CDK v1.1.0, the latest version at the time of development.

## Build
```
npm run build
```

## Deploy
You will be prompted to confirm deployment if there are any changes to resource permissions.
```
cdk deploy
```

## Destroy
```
cdk destroy
```
